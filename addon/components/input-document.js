import TextField from '@ember/component/text-field';
import {
  computed,
  observer
} from '@ember/object';

export default TextField.extend({

  didRender() {
    this._super(...arguments);
    this._formatDocument();
  },

  unmaskedValue: computed('value', function () {

    let value = this.get('value');

    if (value) {
      return value.replace(/[^\d]+/g, '');
    }

    return '';
  }),

  onChange: observer('value', function () {
    this._formatDocument();
  }),

  _formatDocument() {

    let value = this.get('value');
    let unmaskedValue = this.get('unmaskedValue');

    if (value) {

      if (unmaskedValue.length <= 11) {

        this.set('value', this._cpfMask(value));
      } else if (unmaskedValue.length > 11 && unmaskedValue.length <= 14) {

        this.set('value', this._cnpjMask(value));
      } else {

        this.set('value', unmaskedValue.substr(0, 14));
      }
    }

  },

  _cpfMask(value) {
    value = value.replace(/\D/g, "");
    value = value.replace(/(\d{3})(\d)/, "$1.$2");
    value = value.replace(/(\d{3})(\d)/, "$1.$2");
    value = value.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
    return value;
  },

  _cnpjMask(value) {

    value = value.replace(/\D/g, "");
    value = value.replace(/^(\d{2})(\d)/, "$1.$2");
    value = value.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
    value = value.replace(/\.(\d{3})(\d)/, ".$1/$2");
    value = value.replace(/(\d{4})(\d)/, "$1-$2");

    return value
  }

});
