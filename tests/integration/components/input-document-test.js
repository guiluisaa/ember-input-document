import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Component | input-document', function(hooks) {
  setupRenderingTest(hooks);

  test('should show a masked cpf number', async function (assert) {
    
    this.set('document', '00000000000');

    await render(hbs`{{input-document value=document}}`);

    assert.equal(this.get('document'), '000.000.000-00');
  });

  test('should show a masked cnpj number', async function (assert) {
    
    this.set('document', '00000000000000');

    await render(hbs`{{input-document value=document}}`);

    assert.equal(this.get('document'), '00.000.000/0000-00');
  });

  test('should show only a masked cnpj number when pass more then 14 characters', async function (assert) {
    
    this.set('document', '00000000000000123');

    await render(hbs`{{input-document value=document}}`);

    assert.equal(this.get('document'), '00.000.000/0000-00');
  });
});
